<?php
namespace Dawidzbizek\Moneytask;

final class Money
{
    public float $amount;
    public Currency $currency;

    public function __construct(string $amount, Currency $currency)
    {
        $this->amount = (float) sprintf('%0.2f', $amount);
        $this->currency = $currency;
    }

    public function __toString() : string
    {
        return number_format($this->getAmount(), 2, '.', ',');
    }

    public function setAmount(float $amount) : Money
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmount() : float
    {
        return $this->amount;
    }

    public function getCurrency() : Currency
    {
        return $this->currency;
    }

    private function assertSameCurrency(Money $money)
    {
        if(!$this->getCurrency()->equals($money->getCurrency()))
            throw new InvalidArgumentException('Currencies must be identical');
    }

    public function add(Money $money) : Money
    {
        $this->assertSameCurrency($money);

        return $this->setAmount($this->getAmount() + $money->getAmount());
    }

    public function subtract(Money $money) : Money
    {
        $this->assertSameCurrency($money);

        return $this->setAmount($this->getAmount() - $money->getAmount());
    }

    public function multiply(float $rate) : Money
    {
        return $this->setAmount($this->getAmount() * $rate);
    }

    public function divide(float $rate) : Money
    {
        if($rate == 0)
            throw new InvalidArgumentException('Cannot be divided by zero');

        return $this->setAmount($this->getAmount() / $rate);
    }
}
?>