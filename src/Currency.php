<?php
namespace Dawidzbizek\Moneytask;

final class Currency
{
    public string $code;

    public function __construct(string $code)
    {
        $this->code = $code;
    }

    public function getCode() : string
    {
        return $this->code;
    }

    public function equals(Currency $currency) : bool
    {
        return $this->getCode() === $currency->getCode();
    }
}
?>