<?php
use Dawidzbizek\Moneytask\Money;
use Dawidzbizek\Moneytask\Currency;
use PHPUnit\Framework\TestCase;

class MoneyTest extends TestCase
{
    public function testInstance()
    {
        $money = new Money('199.99', new Currency('PLN'));

        $this->assertInstanceOf(Money::class, $money);
    }

    public function testGetCurrency()
    {
        $money = new Money('100', new Currency('PLN'));

        $this->assertInstanceOf(Currency::class, $money->getCurrency());
    }

    public function testGetAmount()
    {
        $money = new Money('100', new Currency('PLN'));

        $this->assertEquals('100.00', $money->getAmount());
    }

    public function testAdd()
    {
        $money = new Money(100, new Currency('USD'));
        $money2 = new Money(100, new Currency('USD'));

        $result = $money->add($money2);

        $this->assertEquals('200.00', $result->getAmount());
    }

    public function testSubtract()
    {
        $money = new Money(100, new Currency('USD'));
        $money2 = new Money(50, new Currency('USD'));

        $result = $money->subtract($money2);

        $this->assertEquals('50.00', $result->getAmount());
    }

    public function testMultiply()
    {
        $money = new Money(100, new Currency('USD'));

        $result = $money->multiply(2);

        $this->assertEquals('200.00', $result->getAmount());
    }

    public function testDivide()
    {
        $money = new Money(200, new Currency('USD'));

        $result = $money->divide(2);

        $this->assertEquals('100.00', $result->getAmount());
    }

    public function testString()
    {
        $money = new Money(19900.99, new Currency('PLN'));

        $this->assertEquals('19,900.99', (string) $money);
    }
}
