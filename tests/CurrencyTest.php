<?php
use Dawidzbizek\Moneytask\Currency;
use PHPUnit\Framework\TestCase;

class CurrencyTest extends TestCase
{
    public function testInstance()
    {
        $currency = new Currency('PLN');

        $this->assertInstanceOf(Currency::class, $currency);
    }

    public function testCode()
    {
        $currency = new Currency('PLN');

        $this->assertEquals('PLN', $currency->getCode());
    }

    public function testEqualsFalse()
    {
        $c1 = new Currency('PLN');
        $c2 = new Currency('USD');

        $this->assertFalse($c1->equals($c2));
    }

    public function testEqualsTrue()
    {
        $c1 = new Currency('PLN');
        $c2 = new Currency('PLN');

        $this->assertTrue($c1->equals($c2));
    }
}
